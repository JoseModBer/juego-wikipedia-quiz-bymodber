`use strict`;
/**
 * ##################################################################################
 * ########### IMPORTAMOS FUNCIONES DE "helpers.js" #################################
 * ##################################################################################
 */
/*Importo haciendo destructuring */
import { getData, shuffleArray, randomItem } from '/js/helpers.js';

/**
 * ##################################################################################
 * ########### SELECCIONO LOS ELEMENTOS A USAR DEL HTML ####################
 * ##################################################################################
 */
// Estos elementos del HTML los voy a usar en la función Nº5 WRITEQUESTION
// Selecciono los elementos por su ID
const points = document.querySelector('#points');
const quiz = document.querySelector('#quiz');

/**
 * ##################################################################################
 * ########### VARIABLE LOCAL EN LA QUE ALMACENAR COSAS GLOBALES ####################
 * ##################################################################################
 */
const STATE = {
  points: 0,
  articles: [],
};

/**
 * ##################################################################################
 * ########### CONSTANTE HIDDENTEXT #########################################
 * ##################################################################################
 */
// Creo esta constante para tapar las respuestas en el cuerpo de la clue.
// Para obtener el █, he buscado en google "utf8 black block" y en el primer link "https://www.fileformat.info/info/unicode/char/2588/index.htm" hice scroll hasta la parte en la que dice "Java Data | string.to.uppercase()" y copié y pegué el bloque negro.
const HIDDENTEXT = '██████████';

/**
 * ##################################################################################
 * ########### 3. FUNCIÓN ASÍNCRONA GETARTICLECATEGORIES ####################
 * ##################################################################################
 */
// Función que hace una petición a la parte de wikipedia para listar las categorías a las que pertenece un artículo de la Wikipedia.
// Luego se queda con los títulos de las categorías PERO descarta aquellos títulos de categorías que incluyen la palabra "Wikipedia" ya que estas se tratan de categorías que no me interesan para el juego.
async function getArticleCategories(article) {
  const data = await getData(
    `https://es.wikipedia.org/w/api.php?format=json&origin=*&action=query&prop=categories&titles=${article}`
  );
  const key = Object.keys(data.query.pages)[0];
  const categories = data.query.pages[key].categories
    .map((category) => category.title)
    .filter((category) => !category.includes('Wikipedia'));
  return categories;
}

/**
 * ##################################################################################
 * ########### 4. FUNCIÓN ASÍNCRONA GETCATEGORYARTICLES ####################
 * ##################################################################################
 */
// Función que va a listar los artículos de una categoría en concreto de la wikipedia (excluyendo el que se pasa por parámetro).
async function getCategoryArticles(category, exclude) {
  const data = await getData(
    `https://es.wikipedia.org/w/api.php?format=json&origin=*&action=query&list=categorymembers&cmlimit=500&cmtitle=${category}`
  );
  // Filtro para que NO devuelva los artículos que tienen dos puntos.
  // Además lo mapeo para que solo me de el title del article.
  const articles = data.query.categorymembers
    .filter(
      (article) => !article.title.includes(':') && article.title !== exclude
    )
    .map((article) => article.title);
  return articles;
}

/**
 * ##################################################################################
 * ########### 2. FUNCIÓN ASÍNCRONA GENERATEQUESTION ####################
 * ##################################################################################
 */
//Función que construye una pregunta con su pista, respuesta y opciones | Para ello va a realizar los siguientes pasos:
async function generateQuestion() {
  quiz.innerHTML = 'Cargando pregunta...';
  try {
    // 2.1. Coge un artículo al azar de STATE.articles.
    const answer = randomItem(STATE.articles);

    // 2.2. Extrae el título y el resumen de ese artículo de la API.
    const { title, extract } = await getData(
      `https://es.wikipedia.org/api/rest_v1/page/summary/${answer}`
    );

    // 2.3. Oculta las palabras del resumen que coincidan con el título.
    const regex = new RegExp(title, 'ig');
    const clue = extract.replaceAll(regex, HIDDENTEXT);

    // 2.4. Si no ocultó ninguna palabra, genera un error.
    if (!clue.includes(HIDDENTEXT)) {
      throw new Error('Cannot find title in extract');
    }
    // 2.5. Busca las categorías del artículo escogido.
    const categories = await getArticleCategories(answer);

    // 2.6. Escoge una categoría.
    const chosenCategory = randomItem(categories);

    // 2.7. Busca otros artículos en es misma categoría.
    const falseOptions = await getCategoryArticles(chosenCategory, title);

    // 2.8 Si falseOptions tiene menos de 3 artículos volvemos a generar la pregunta
    if (falseOptions.length < 3) {
      throw new Error('Not enought false options to play.');
    }

    // 2.9. Escoge 3 artículos relaccionados.
    const options = shuffleArray(falseOptions).slice(0, 3);

    // 2.10 Llegados a este punto tengo el título  del artículo (respuesta correcta), la pista con la zona en negro y las opciones falsas.
    /*       console.log(title);
      console.log(clue);
      console.log(options); */

    // 2.11 Creo la constante del quiz "question", que contendrá la pregunta (clue) y cuatro respuestas (la correcta + otras tres relaccionadas por categoría).
    const question = {
      clue: clue,
      answer: title,
      options: shuffleArray([...options, title]),
    };
    console.log(question);
    // Llamo a la función Nº5 "writeQuestion" dándole como valor la constante question anterior.
    writeQuestion(question);
  } catch (err) {
    generateQuestion();
  }
}

/**
 * ##################################################################################
 * ########### 5. FUNCIÓN WRITEQUESTION ####################
 * ##################################################################################
 */
// Función normal que recibe la constante question y la escribe en el html.
function writeQuestion(question) {
  // 1. Borro lo que haya en el main del html (borra el contenido de la pregunta actual si la hay, si no, borra el "cargando...").
  quiz.innerHTML = '';
  // 2. Acualizo los puntos (los cojo del STATE):
  points.innerText = STATE.points;
  // 3. Escribo la pista (creo un párrafo que contendrá la clue):
  const clue = document.createElement('p');
  clue.innerText = question.clue;
  quiz.append(clue);
  // Escribo las respuestas | Recorro el objeto question, sección options con un bucle for y creo botones con las respuestas | Le doy funcionalidad a los botones según tengan la respuesta correcta o incorrecta | Hago que STATE.points vaya acumulando puntos en caso de acertar o que reste un punto en caso de no acertar:
  for (const answer of question.options) {
    const answerButton = document.createElement('Button');
    answerButton.innerText = answer;
    answerButton.onclick = () => {
      if (answer === question.answer) {
        // Si el botón contiene la respuesta correcta:
        alert('Respuesta correcta!! Ganas un punto.');
        if (STATE.points === 9) {
          confirm(`Ganaste! Has alcanzado los 10 puntos.`);
          STATE.points = 0;
        } else {
          STATE.points++;
        }
      } else {
        // Si el botón NO contiene la respuesta correcta:
        alert('Respuesta incorrecta. Pierdes un punto...');
        if (STATE.points === 0) {
          STATE.points = 0;
        } else {
          STATE.points--;
        }
      }
      generateQuestion();
    };
    quiz.append(answerButton);
  }
}

/**
 * ##################################################################################
 * ########### 1. FUNCIÓN ASÍNCRONA START ####################
 * ##################################################################################
 */
// Función que genera la fecha de ayer y con ella hace una petición a la api para obtener los títulos de los artículos que me interesen y los cargue en el objeto global STATE definido anteriormente (Función que carga la lista inicial de preguntas).
async function start() {
  try {
    /**
     * ###################################################################
     * ########### CONSIGUIENDO LA FECHA DE AYER #########################
     * ###################################################################
     */
    // CONSIGUIENDO LA FEHA DE AYER PARA USARLA EN LA URL DE LA PETICIÓN MÁS ADELANTE:
    // Declaro la constante yesterday para conseguir la Date de ayer.
    const yesterday = new Date(Date.now() - 86400000);
    //console.log(yesterday);

    // Extraigo y guardo en constantes el año, mes y día de ayer.
    const year = yesterday.getFullYear();
    // En el caso del mes y el día, los convierto en string para poder añadir luego ceros para el formato de fecha.
    // Además les meto un padStart para decirle que siempre que no tenga dos cifras, rellen por delante con un cero.
    const month = String(yesterday.getMonth() + 1).padStart(2, '0');
    const day = String(yesterday.getDate()).padStart(2, '0');
    //console.log(year, month, day);

    /**
     * ###################################################################
     * ########### PETICIÓN A LA URL CON ARTÍCULOS #######################
     * ###################################################################
     */
    // Uso la función getData definida en helpers.js
    const data = await getData(
      `https://wikimedia.org/api/rest_v1/metrics/pageviews/top/es.wikipedia.org/all-access/${year}/${month}/${day}`
    );
    //console.log(`Contenido de data:`, data);

    // Filtro los artículos validos, es decir, todos los de items[0].articles que no incluyan dos puntos.
    const validArticles = data.items[0].articles.filter(
      (item) => !item.article.includes(':')
    );
    //console.log(`data filtrada (validArticles):`, validArticles);

    // Filtro los artículos validados para que solo devuelvan el título del artículo.
    const articles = validArticles.map((item) => item.article);
    //console.log(`Título de los artículos validados:`, articles);
    // Cargo el array de nombres de artículos en el objeto global STATE.articles creado al ppio.
    STATE.articles = articles;
    //console.log(STATE.articles);

    // Llamo a la función generateQuestion.
    generateQuestion();
  } catch (err) {
    alert(
      `Desactiva el adblock para poder jugar (Adblock da un falso positivo pero no hay publicidad en esta app 😉). | Si ya has desactivado Adblock y este mensaje persiste, se debe a que los servidores de wikipedia están en mantenimiento. Intentalo de nuevo más tarde.`
    );
  }
}
// Llamo a la función Nº1 start.
start();
