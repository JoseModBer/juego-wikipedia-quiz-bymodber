`use strict`;

// En este documento defino varias funciones y las exporto para poder importarlas en el main.js y poder llamarlas cuando quiera.

/**
 * ##################################################################################
 * ########### FUNCIÓN "getData" QUE DEVUELVE EL CONTENIDO DE UNA URL ###############
 * ##################################################################################
 */
export async function getData(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

/**
 * ##################################################################################
 * ##### FUNCIÓN "shuffleArray" QUE RECIBE UN ARRAY Y LO DEVUELVE ALEATORIZADO ######
 * ##################################################################################
 */
export const shuffleArray = (a) => a.sort(() => Math.random() - 0.5);

/**
 * ##################################################################################
 * ##### FUNCIÓN "randomItem" QUE RECIBE UN ARRAY Y DEVUELVE UN ELEMENTO DE ESTE ####
 * ##################################################################################
 */
export const randomItem = (a) => a[Math.floor(Math.random() * a.length)];
